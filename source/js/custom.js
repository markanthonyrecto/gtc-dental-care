'use strict';
var gtc = {
	width: 0,
	height: 0,
	headerHeight: 0,
	footerHeight: 0,
	isMobile: false,
	init: function() {
		gtc.mobileDetect();
		// for mobile
		if (gtc.isMobile) {
			document.querySelector('html').classList.add('bp-touch');
		}

		gtc.resizeFn(function() {
			gtc.resize();
		});

		// for ready
		$(document).ready(function() {
			gtc.ready();
		});

		// for header
		gtc.header();

		// for footer
		gtc.footer({target: document.querySelector('.footer-content')});

		// for inview
		gtc.inview();

		// for text field
		$('.com_text-field').each(function() {
			gtc.textField({target: this});
		});

		// for aspect ratio
		$('.com_aspect-ratio').each(function() {
			gtc.aspectRatio({target: this});
		});

		// for parallax items
		$('.com_anim-keyframe').each(function() {
			gtc.animKeyframe({target: this});
		});

		// for before and after
		$('.com_twentytwenty').each(function() {
			var _this = this,
			_images = this.querySelectorAll('img'),
			_loaded = false,
			_interval = '';

			for (var i = 0; i < _images.length; i++) {
				var _img = new Image();
				_img.src = _images[i].getAttribute('src');

				_img.onload = function() {
					_loaded = true;
				}

			}

			_interval = setInterval(function() {
				if(_loaded) {
					clearInterval(_interval);

					$(_this).twentytwenty();
				}
			}, 10);			
		});

		// for date picker
		$('.com_date-picker').each(function() {
			var _this = this,
			_input = _this.querySelector('input');

			$(_input).daterangepicker({
				singleDatePicker: true,
				timePicker: true,
				startDate: moment().startOf('hour'),
				locale: {
					format: 'MM/DD/YY hh:mm A'
				},
				autoUpdateInput: false
			});

			$(_input).on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('MM/DD/YY - hh:mm A'));
			});

			$(_input).on('cancel.daterangepicker', function(ev, picker) {
				$(this).val('');
			});
		});

		// for mobile slider
		$('.com_sneaky-carousel').each(function() {
			var _carousel = this,
			_behavior = 'responsive',
			_autoplay = false,
			_autoplayInterval = '',
			_nav = true,
			_dots = true,
			_loop = false,
			_items = _carousel.querySelectorAll('.item');

			gtc.getAttr({
				target: _carousel,
				attr: 'sneakyCarousel',
				after: function(attr) {
					if(attr[0] == 'mobile') {
						_behavior = 'mobile';
					}

					if(attr[1] == 'false') {
						_nav = false;
					}

					if(attr[2] == 'false') {
						_dots = false;
					}

					if(attr[3] == 'autoplay') {
						_autoplay = true;
					}

					if(attr[4] == 'loop') {
						_loop = true;
					}
				}
			});

			// if items are insufficient
			if(_items.length <= 1) {
				_nav = false;
				_dots = false;
			}

			var _properties = {
				autoplay: _autoplay,
				loop: _loop,
				margin: 24,
				items: 1,
				nav: _nav,
				dots: _dots,
				navText: ['',''],
				// smartSpeed: 600,
				autoplaySpeed: 700,
				navSpeed: 700,
				dotsSpeed: 700,
				responsive: {
					0: {
						items: 1
					},
					600: {
						items: 2
					}
				}
			}

			if(_behavior == 'responsive') {
				$(_carousel).addClass('owl-carousel');
				$(_carousel).owlCarousel(_properties);
			} else {
				gtc.enquire({
					between: 767,
					desktop: function() {
						$(_carousel).removeClass('owl-carousel');
						$(_carousel).owlCarousel('destroy');
					},
					mobile: function() {
						$(_carousel).addClass('owl-carousel');
						$(_carousel).owlCarousel(_properties);
					}
				});
			}

			// after click on carousel, temporary disable the autoplay
			$(_carousel).click(function() {
				if(_autoplay) {
					$(_carousel).trigger('stop.owl.autoplay');
					clearInterval(_autoplayInterval);

					var _count = 0;
					_autoplayInterval = setInterval(function() {
						_count++;

						if(_count == 50) {
							clearInterval(_autoplayInterval);
							$(_carousel).trigger('play.owl.autoplay');
						}
					}, 10);
				}
			});
		});
	},
	ready: function() {
		gtc.resize();
	},
	resize: function() {
		var _resize = {
			init: function() {
				gtc.width = window.innerWidth;
				gtc.height = window.innerHeight;

				// STICKY FOOTER
				var header = document.querySelector('.header-content'),
				footer = document.querySelector('footer');

				gtc.headerHeight = header.offsetHeight;
				gtc.footerHeight = footer.offsetHeight;

				$(footer).css({marginTop: -(gtc.footerHeight)});
				$('#main-wrapper').css({paddingBottom: gtc.footerHeight});

				// for equal height
				$('.group-height').each(function() {
					gtc.equalize(this.querySelectorAll('.gh1'));
				});

				var _firstSection = $('.section')[0];
				if(_firstSection) {
					$('.container-fluid', _firstSection).first().css({paddingTop: gtc.headerHeight});
				}

				$('.com_main-height').css({minHeight: gtc.height - gtc.footerHeight});
			}
		}
		_resize.init();
	},
	equalize: function(target) {
		for (var i = 0; i < target.length; i++) {
			target[i].style.minHeight = 0;
		}

		var _biggest = 0;
		for (var i = 0; i < target.length; i++ ){
			var element_height = target[i].offsetHeight;
			if(element_height > _biggest ) _biggest = element_height;
		}

		for (var i = 0; i < target.length; i++) {
			target[i].style.minHeight = _biggest + 'px';
		}
		
		return _biggest;
	},
	mobileDetect: function() {
		var isMobile = {
		    Android: function() {
		        return navigator.userAgent.match(/Android/i);
		    },
		    BlackBerry: function() {
		        return navigator.userAgent.match(/BlackBerry/i);
		    },
		    iOS: function() {
		        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		    },
		    Opera: function() {
		        return navigator.userAgent.match(/Opera Mini/i);
		    },
		    Windows: function() {
		        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
		    },
		    any: function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		    }
		};
		if(isMobile.any) {
			gtc.isMobile = isMobile.any();
		}
	},
	resizeFn: function(fnctn, load) {
		var _resizeTimer = '';

		fnctn();
		window.addEventListener('resize', function() {
			fnctn();

			clearTimeout(_resizeTimer);
			_resizeTimer = setTimeout(function() {
				fnctn('after');
			}, 300);
		});

		$(document).ready(function() {
			fnctn('ready');
		});

		if(load) {
			window.addEventListener('load', function() {
				fnctn('ready');
			});
		}
	},
	loadFn: function(fnctn) {
		if(document.readyState === 'interactive' || document.readyState === 'complete') {
			return fnctn();
		} else if(document.addEventListener) {
			document.addEventListener('DOMContentLoaded', function() {
				return fnctn();
			});
		} else if (document.attachEvent) {
			document.attachEvent("onreadystatechange", function() {
				if(document.readyState != 'loading') {
					return fnctn();
				}
			});
		}
	},
	scrollFn: function(fnctn) {
		fnctn();
		window.addEventListener('scroll', function(e) {
			fnctn(e);
		});
	},
	inview: function(state) {
		var _animate = {
			init: function() {
				$('.animate').each(function() {
					var _state = true,
					_this = $(this),
					_animControl = _this.attr('anim-control'),
					_animName = _this.attr('anim-name'),
					_animNameMobile = _this.attr('anim-name-mobile'),
					_animDelay = Math.floor(_this.attr('anim-delay')),
					_delayIncrease = 0.2,
					_delayNum = 0;

					// set the css animation name
					if(!(_animName)) {
						_animName = 'anim-content';
					} else {
						// if there is a special animation name
						if(ocbc.width > ocbc.mobileWidth) {
							// for desktop
							_animName = _animName + ' animated';
						} else {
							// for mobile
							if(!(_animNameMobile)) {
								_animName = _animName + ' animated';
							} else {
								// if there is animation name for mobile
								_animName = _animNameMobile + ' animated';
							}
						}
					}

					// set the animation delay
					if(!(_animDelay)) {
						// if there is no attr for delay
						_animDelay = 0.5;
					}
					_delayNum = _animDelay + 's';

					if(_animControl == 'parent') {
						// for parent container
						var _childen = $('> *', _this);

						_childen.each(function(i) {
							var _child = $(this);

							// multiply the delay depending on child count
							_delayNum = (_animDelay + (_delayIncrease * i)) + 's';

							_child.css('animation-delay', _delayNum);

							// once the target is on display
							$(_this).on('inview', function(event, visible) {
								if(_state) {
									_state = false;

									if(visible) {
										$(_this).removeClass('animate');
										_childen.addClass('animate');
										_childen.addClass(_animName + ' visible');

										// once done, remove everything related to animation
										setTimeout(function() {
											_childen.removeClass('animate animated anim-content visible');
											_childen.removeClass(_animName);
											_childen.css('animation-delay', '');
										}, ((_delayIncrease * _childen.length) * 1000) + (_animDelay * 1000) * 2);
									}
								}
							});
						});
					} else {
						// for single container
						_this.css('animation-delay', _delayNum);
						
						// once the target is on display
						$(_this).on('inview', function(event, visible) {
							if(_state) {
								_state = false;

								if(visible) {
									$(_this).addClass(_animName + ' visible');

									// once done, remove everything related to animation
									setTimeout(function() {
										$(_this).removeClass('animate animated anim-content visible');
										$(_this).removeClass(_animName);
										$(_this).css('animation-delay', '');
									}, (_animDelay * 1000) * 2);
								}
							}
						});
					}
				});
			}
		}

		if(state == 'destroy') {
			$('.animate').removeClass('animate');
		} else {
			(function(d){var p={},e,a,h=document,i=window,f=h.documentElement,j=d.expando;d.event.special.inview={add:function(a){p[a.guid+"-"+this[j]]={data:a,$element:d(this)}},remove:function(a){try{delete p[a.guid+"-"+this[j]]}catch(d){}}};d(i).bind("scroll resize",function(){e=a=null});!f.addEventListener&&f.attachEvent&&f.attachEvent("onfocusin",function(){a=null});setInterval(function(){var k=d(),j,n=0;d.each(p,function(a,b){var c=b.data.selector,d=b.$element;k=k.add(c?d.find(c):d)});if(j=k.length){var b;
			if(!(b=e)){var g={height:i.innerHeight,width:i.innerWidth};if(!g.height&&((b=h.compatMode)||!d.support.boxModel))b="CSS1Compat"===b?f:h.body,g={height:b.clientHeight,width:b.clientWidth};b=g}e=b;for(a=a||{top:i.pageYOffset||f.scrollTop||h.body.scrollTop,left:i.pageXOffset||f.scrollLeft||h.body.scrollLeft};n<j;n++)if(d.contains(f,k[n])){b=d(k[n]);var l=b.height(),m=b.width(),c=b.offset(),g=b.data("inview");if(!a||!e)break;c.top+l>a.top&&c.top<a.top+e.height&&c.left+m>a.left&&c.left<a.left+e.width?
			(m=a.left>c.left?"right":a.left+e.width<c.left+m?"left":"both",l=a.top>c.top?"bottom":a.top+e.height<c.top+l?"top":"both",c=m+"-"+l,(!g||g!==c)&&b.data("inview",c).trigger("inview",[!0,m,l])):g&&b.data("inview",!1).trigger("inview",[!1])}}},250)})(jQuery);

			gtc.loadFn((function() {
				_animate.init();
			}));
		}
	},
	st: function() { return window.pageYOffset || document.documentElement.scrollTop },
	getAttr: function(opt) {
		var _attr = eval('opt.target.dataset.' + opt.attr);

		if(_attr) {
			if(opt.after) {
				if(opt.array) {
					opt.after(JSON.parse(_attr));
				} else {
					opt.after(_attr.split(','));
				}
			}
		}
	},
	enquire: function(opt) {
		var _enquire = {
			status: 'once',
			desktop: opt.desktop,
			mobile: opt.mobile,
			counterDesktop: 0,
			counterMobile: 0,
			width: 0,
			between: 810,
			init: function() {
				if(opt.between) {
					_enquire.between = opt.between;
				}

				if(opt.status) {
					_enquire.status = opt.status;
				}

				_enquire.update();
				window.addEventListener('resize', function() {
					_enquire.update();
				});
			},
			update: function() {
				_enquire.width = $(window).outerWidth();

				if(_enquire.status == 'always') {
					// update every resize
					if(_enquire.width > _enquire.between) {
						_enquire.desktop();
					} else {
						_enquire.mobile();
					}
				} else {
					// if detected desktop or mobile then update once
					if(_enquire.width > _enquire.between) {
						_enquire.counterMobile = 0;

						// if desktop
						if(_enquire.counterDesktop == 0) {
							_enquire.counterDesktop = 1;
							_enquire.desktop();
						}
					} else {
						_enquire.counterDesktop = 0;

						// if mobile
						if(_enquire.counterMobile == 0) {
							_enquire.counterMobile = 1;
							_enquire.mobile();
						}
					}
				}
			}
		}
		_enquire.init();
	},
	dispatchEvent: function(elem, eventName) {
		var event;
		if (typeof(Event) === 'function') {
			event = new Event(eventName);
		}
		else {
			event = document.createEvent('Event');
			event.initEvent(eventName, true, true);
		}
		elem.dispatchEvent(event);
	},
	header: function() {
		var _header = {
			target: '',
			lastScrollTop: 0,
			init: function() {
				_header.target = document.querySelector('.header-content');
				_header.menu.bar = document.querySelector('.menu-bar');
				_header.menu.main = document.querySelector('.mobile-menu.main');
				_header.menu.cover = document.querySelector('.mobile-menu.cover');

				$(_header.menu.bar).click(function() {
					if($(this).hasClass('active')) {
						_header.menu.hide();
					} else {
						_header.menu.show();
					}
				});

				gtc.scrollFn(function() {
					_header.scrolling();
				});

				gtc.resizeFn(function() {
					_header.resize();
					_header.scrolling();
				});

				gtc.enquire({
					between: 768,
					desktop: function() {
						if($('html').hasClass('menu-mobile-active')) {
							_header.menu.hide();
						}
					},
					mobile: function() {
						if($('html').hasClass('menu-mobile-active')) {
							_header.menu.hide();
						}
					}
				});
			},
			scrolling: function() {
				var _st = gtc.st(),
				_range = _st >= gtc.headerHeight * 0.9;

				if(_range) {
					_header.target.classList.add('header-down');
				} else {
					_header.target.classList.remove('header-down');
				}

				if(_st > _header.lastScrollTop) {
					// scrolling down
					if(_st >= gtc.headerHeight) {
						if($(_header.target).hasClass('header-absolute')) {
							TweenMax.set(_header.target, {y: -(gtc.headerHeight)});
						} else {
							TweenMax.to(_header.target, 0.3, {y: -(gtc.headerHeight)});
						}

						$(_header.target).removeClass('header-absolute');
					} else {
						$(_header.target).addClass('header-absolute');
					}
				} else {
					// scrolling up
					TweenMax.to(_header.target, 0.3, {y: 0});
				}

				_header.lastScrollTop = _st <= 0 ? 0 : _st;
			},
			menu: {
				bar: '',
				main: '',
				cover: '',
				show: function() {
					$('html').addClass('menu-mobile-active');
					$(_header.menu.bar).addClass('active');

					gtc.dispatchEvent(window, 'resize');

					$(_header.menu.cover).show();
					TweenMax.fromTo(_header.menu.cover, 0.5, {height: 0}, {height: window.innerHeight, onComplete: function() {
						$(_header.menu.cover).css({height: ''});
					}});

					$(_header.menu.main).show();
					TweenMax.set($('li', _header.menu.main), {opacity: 0, x: 40});
					TweenMax.staggerTo($('li', _header.menu.main), 0.5, {opacity: 1, x: 0}, 0.05);
				},
				hide: function() {
					$('html').removeClass('menu-mobile-active');
					$(_header.menu.bar).removeClass('active');

					TweenMax.to(_header.menu.cover, 0.5, {height: 0, onComplete: function() {
						$(_header.menu.cover).hide();
						$(_header.menu.main).hide();
					}});

					TweenMax.staggerTo($('li', _header.menu.main), 0.5, {opacity: 0}, -0.05);
				}
			},
			resize: function() {
				if($('html').hasClass('menu-mobile-active')) {
					$(_header.menu.main).css({paddingTop: gtc.headerHeight});
				}
			}
		}
		_header.init();
	},
	footer: function(opt) {
		var _footer = {
			target: '',
			init: function() {
				_footer.target = opt.target;

				gtc.resizeFn(function() {
					_footer.scrolling();
				});

				gtc.scrollFn(function() {
					_footer.scrolling();
				});
			},
			scrolling: function() {
				var _offsetTop = gtc.st() - (_footer.target.parentNode.offsetTop - window.innerHeight),
				_footerHeight = _footer.target.offsetHeight;

				if(window.innerHeight <= _footerHeight) {
					$(_footer.target).css({transform: ''});
				} else {
					if(_offsetTop <= 0) {
						TweenMax.set(_footer.target, {y: '-50%'});

					} else {
						var _pullTop = 50 - ((_offsetTop / _footerHeight) * 50);
						TweenMax.set(_footer.target, {y: '-'+_pullTop+'%'});
					}
				}
			}
		}

		if(opt.target) {
			_footer.init();
		}
	},
	textField: function(opt) {
		var _text = {
			target: '',
			input: '',
			limit: 200,
			highlight: false,
			init: function() {
				_text.target = opt.target;
				_text.input = _text.target.querySelector('textarea');

				// get the text max length
				var _maxLength = parseInt(_text.input.getAttribute('maxlength'));
				if(_maxLength) {
					_text.limit = _maxLength;
				}

				// highlight the text
				if(_text.input.value.length != 0) {
					_text.input.addEventListener('click', function() {
						var strLength = $(_text.input).val().length * 2;
						$(_text.input).focus();						
						$(_text.input)[0].setSelectionRange(0, strLength);
					});
				}

				// text limit control
				_text.input.addEventListener('keypress', function(e) {
					if(this.value.length >= _text.limit) {
						e.preventDefault();
					}
				});

				// paste limit control
				_text.input.addEventListener('paste', function(e) {
					e.clipboardData.getData('text/plain').slice(0, _text.limit);
				});

				_text.input.addEventListener('input', function() {
					_text.resize();
				});

				gtc.resizeFn(function() {
					_text.resize();
				});
			},
			resize: function() {
				_text.input.style.minHeight = '';
				_text.input.style.minHeight = (_text.input.scrollHeight + 2) + 'px';
			}
		}
		_text.init();
	},
	aspectRatio: function(opt) {
		var _ar = {
			target: '',
			base: {
				width: 0,
				height: 0,
				maxWidth: false,
				maxHeight: false,
				ratio: 0,
			},
			current: {
				width: 0,
				height: 0
			},
			init: function() {
				_ar.target = opt.target;

				gtc.getAttr({
					target: _ar.target,
					attr: 'aspectRatio',
					after: function(attr) {
						_ar.base.width = parseInt(attr[0]);
						_ar.base.height = parseInt(attr[1]);

						if(attr[2] == 'maxWidth') {
							_ar.base.maxWidth = true;
						}

						if(attr[2] == 'maxHeight') {
							_ar.base.maxHeight = true;
						}

						if(attr[3]) {
							_ar.base.ratio = parseInt(attr[3].replace('%', '')) / 100;
						}
					}
				});

				gtc.resizeFn(function() {
					_ar.update();
				});
			},
			update: function() {
				var _parentWidth = _ar.target.parentNode.offsetWidth,
				_parentHeight = _ar.target.parentNode.offsetHeight;

				if(_parentWidth <= 0) {
					// if parents width is 0, use base width
					_ar.current.width = _ar.base.width;
				} else {
					// use parents width
					_ar.current.width = _parentWidth;
				}

				// if need to control maxWidth
				if(_ar.base.maxWidth) {
					if(_ar.current.width >= _ar.base.width) {
						_ar.current.width = _ar.base.width;
					}
				}

				if(_ar.base.maxHeight) {
					// prioritize the base height
					_ar.current.height = _ar.target.parentNode.offsetHeight * _ar.base.ratio;

					_ar.target.style.width = ( _ar.base.width * (_ar.current.height / _ar.base.height) ) + 'px';
					_ar.target.style.height = _ar.current.height + 'px';
				} else {
					// usual resize
					_ar.target.style.width = _ar.current.width + 'px';
					_ar.target.style.height =  ( _ar.base.height * (_ar.current.width / _ar.base.width) ) + 'px';
				}
			}
		}
		_ar.init();
	},
	animKeyframe: function(opt) {
		var _anim = {
			target: '',
			frames: '',
			axis: 0,
			defOffset: 0.5,
			init: function() {
				_anim.target = opt.target;

				gtc.getAttr({
					target: _anim.target,
					attr: 'animKeyframe',
					array: true,
					after: function(attr) {
						if(attr) {
							_anim.frames = attr;
						}
					}
				});

				gtc.resizeFn(function() {
					_anim.axis = 0;

					for (var i = 0; i < _anim.frames.length; i++) {
						var _thisFrame = _anim.frames[i],
						_thisWidth = parseInt(Object.keys(_thisFrame)[0]),
						_thisAxis = parseInt(Object.values(_thisFrame)[0]);

						if(gtc.width < _thisWidth) {
							_anim.axis = _thisAxis;
						}
					}

					_anim.update();
				}, true);

				gtc.scrollFn(function() {
					_anim.update();
				});

				_anim.update();
			},
			update: function() {
				var _offsetTop = $(_anim.target).offset().top;
				_top = _offsetTop - gtc.st();

				TweenMax.set(_anim.target, {y: 0});

				if(_top <= gtc.height *_anim.defOffset) {
					// above screen
					var _scrolled = _top - gtc.height *_anim.defOffset,
					_range = gtc.height *_anim.defOffset,
					_computation = (_scrolled * -1) / _range;

					// if(_computation >= 1) {
					// 	_computation = 1;
					// }

					TweenMax.set(_anim.target, {y: -(_anim.axis * _computation)});
				} else {
					// below screen
					var _scrolled = _top - (gtc.height *_anim.defOffset),
					_range = gtc.height *_anim.defOffset,
					_computation = _scrolled / _range;

					if(_computation >= 1) {
						_computation = 1;
					}

					TweenMax.set(_anim.target, {y: _anim.axis * _computation});
				}
			}
		}
		_anim.init();
	}
}
gtc.init();